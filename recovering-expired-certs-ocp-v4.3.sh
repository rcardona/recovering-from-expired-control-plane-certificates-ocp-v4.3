#!/bin/bash

source env_local.sh

# O - Testing connectivity
echo -e "\n\e[1;34m 0 - TESTING PREREQUISITES \e[0m"

echo -e "\n\e[1;34m 0.0 - Testing connectivity to master-0 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
ssh core@${MASTER_0} 'sudo touch /root/test; sudo rm -rf /root/test'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESS => connectivity to master-0 \e[0m"
else
  echo -e "\n\e[1;31m STATUS: FAILED => connectivity to master-0 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 0.1 - Testing connectivity to master-1 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
ssh core@${MASTER_1} 'sudo touch /root/test; sudo rm -rf /root/test'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESS => connectivity to master-1 \e[0m"
else
  echo -e "\n\e[1;31m STATUS: FAILED => connectivity to master-1 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 0.2 - Testing connectivity to master-2 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
ssh core@${MASTER_2} 'sudo touch /root/test; sudo rm -rf /root/test'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESS => connectivity to master-2 \e[0m"
else
  echo -e "\n\e[1;31m STATUS: FAILED => connectivity to master-2 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 0.3 - Testing connectivity to worker-0 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
ssh core@${WORKER_0} 'sudo touch /root/test; sudo rm -rf /root/test'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESS => connectivity to worker-0 \e[0m"
else
  echo -e "\n\e[1;31m STATUS: FAILED => connectivity to worker-0 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 0.4 - Testing connectivity to worker-1 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
ssh core@${WORKER_1} 'sudo touch /root/test; sudo rm -rf /root/test'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESS => connectivity to worker-1 \e[0m"
else
  echo -e "\n\e[1;31m STATUS: FAILED => connectivity to worker-1 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 0.5 - Testing connectivity to worker-2 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
ssh core@${WORKER_2} 'sudo touch /root/test; sudo rm -rf /root/test'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESS => connectivity to worker-2 \e[0m"
else
  echo -e "\n\e[1;31m STATUS: FAILED => connectivity to worker-2 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 0.6 - Testing setting of variable KUBECONFIG_FILE \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
if [[ -z $KUBECONFIG_FILE ]]
then
  echo -e "\n\e[1;31m STATUS: FAILED => varible KUBECONFIG_FILE yet unset, please set in env_local.sh \e[0m\n"
  exit 1
else
  echo -e "\n\e[1;32m STATUS: SUCCESS => varible KUBECONFIG_FILE set \e[0m"
fi

echo -e "\n\e[1;34m 0.7 - Testing setting of variable RELEASE_IMAGE (cluster-kube-apiserver-operator) \e[0m"
echo -e "\e[1;34m Starting ...  \e[0m"
if [[ -z $RELEASE_IMAGE ]]
then
  echo -e "\n\e[1;31m STATUS: FAILED => varible RELEASE_IMAGE yet unset, please use get_OPERATOR_IMAGE_VERSION.sh to find it out \e[0m\n"
  exit 1
else
  echo -e "\n\e[1;32m STATUS: SUCCESS => varible RELEASE_IMAGE set \e[0m"
fi

# 1 - Copying OCP enviroment variables to master-0

echo -e "\n\e[1;34m 1 - COPYING OCP ENVIRONMENT TO MASTER-0 \e[0m"

echo -e "\n\e[1;34m 1.1 - Copying kubeconfig to master-0 \e[0m"
echo -e "\e[1;34m Starting ...  \e[0m"
ssh core@${MASTER_0} 'mkdir -p /tmp/recovering-certs/.kube'
scp ${KUBECONFIG_FILE} core@${MASTER_0}:/tmp/recovering-certs/.kube/config > /tmp/kubeconfig_copy.log
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESS => kubeconfig/config file successfully copied to master-0 \e[0m"
else
  echo -e "\n\e[1;31m `cat /tmp/kubeconfig_copy.log`"
  echo -e "\e[1;31m STATUS: FAILED => kubeconf/config not found (${KUBECONFIG_FILE}) \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 1.2 - Copying recovery remote environment file to master-0 \e[0m"
echo -e "\e[1;34m Starting ...  \e[0m"
echo "export RELEASE_IMAGE=quay.io/openshift-release-dev/ocp-release:${RELEASE_IMAGE}-x86_64" >> ${ENV_REMOTE}
scp ${ENV_REMOTE} core@${MASTER_0}:/tmp/recovering-certs/env_remote.sh > /tmp/copy_env_remote.log
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESS => recovery remote environment file successfully copied to master-0 \e[0m"
else
  echo -e "\n\e[1;31m `cat /tmp/copy_env_remote.log`"
  echo -e "\e[1;31m STATUS: FAILED => recovery remote environment file not found (${ENV_REMOTE}) \e[0m"
  exit 1
fi

# 2 - Obtain the cluster-kube-apiserver-operator image reference

echo -e "\n\e[1;34m 2 - OBTAIN THE cluster-kube-apiserver-operator (KAO_IMAGE) IMAGE REFERENCE \e[0m"
echo -e "\e[1;34m Starting ...  \e[0m \n"
ssh core@${MASTER_0} 'source /tmp/recovering-certs/env_remote.sh; sudo oc adm release info --registry-config='/var/lib/kubelet/config.json' ${RELEASE_IMAGE} --image-for=cluster-kube-apiserver-operator' > /tmp/KAO_IMAGE
if [[ -s /tmp/KAO_IMAGE ]]
then
  echo "export KAO_IMAGE=`cat /tmp/KAO_IMAGE`" >> ${ENV_REMOTE}
  scp ${ENV_REMOTE} core@${MASTER_0}:/tmp/recovering-certs/env_remote.sh > /tmp/env_remote.log
  echo -e "\e[1;34m KAO_IMAGE => `cat /tmp/KAO_IMAGE` \e[0m"
  echo -e "\n\e[1;32m STATUS: SUCCESS => cluster-kube-apiserver-operator image successfully found \e[0m"
else
  echo -e "\n\e[1;31m RELEASE_IMAGE set to ${RELEASE_IMAGE} \e[0m"
  echo -e "\e[1;31m Example of expected format/value for RELEASE_IMAGE => quay.io/openshift-release-dev/ocp-release:4.2.8 \e[0m"
  echo -e "\n\e[1;31m STATUS: FAILED => invalid RELEASE IMAGE, cluster-kube-apiserver-operator image not found \e[0m\n"
  exit 1
fi

# 3 - Pull the cluster-kube-apiserver-operator image

echo -e "\n\e[1;34m 3 - PULL THE cluster-kube-apiserver-operator IMAGE \e[0m"
echo -e "\e[1;34m Starting ...  \e[0m \n"
ssh core@${MASTER_0} 'source /tmp/recovering-certs/env_remote.sh; sudo podman pull --authfile=/var/lib/kubelet/config.json "${KAO_IMAGE}"'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESS => cluster-kube-apiserver-operator image downloaded \e[0m"
else
  echo -e "\n\e[1;31m STATUS: FAILED => cluster-kube-apiserver-operator image not downloaded \e[0m\n"
  exit 1
fi

# 4 - Create a recovery server

echo -e "\n\e[1;34m 4 - CREATE A RECOVERY API SERVER \e[0m"

echo -e "\n\e[1;34m 4.1 - Depuring previous Recovery API Server \e[0m"
echo -e "\e[1;34m Starting ...  \e[0m"
ssh core@${MASTER_0} 'source /tmp/recovering-certs/env_remote.sh; sudo podman run -it --network=host -v /etc/kubernetes/:/etc/kubernetes/:Z --entrypoint=/usr/bin/cluster-kube-apiserver-operator "${KAO_IMAGE}" recovery-apiserver destroy' > /tmp/recoveryApiServerDestroy.log
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m INFO: successful destroy of Recovery API Server \e[0m"
else
  echo -e "\n\e[1;31m `cat /tmp/recoveryApiServerDestroy.log`"
  echo -e "\n\e[1;31m INFO: Recovery API Server not found \e[0m"
fi

echo -e "\n\e[1;34m 4.2 - Provisioning Recovery API Server \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
echo -e "\e[1;34m (it might take several minutes) \e[0m"
ssh core@${MASTER_0} 'source /tmp/recovering-certs/env_remote.sh; sudo podman run -it --network=host -v /etc/kubernetes/:/etc/kubernetes/:Z --entrypoint=/usr/bin/cluster-kube-apiserver-operator "${KAO_IMAGE}" recovery-apiserver create' > /tmp/RECOVERSERVER_KUBECONFIG
if [[ -s /tmp/RECOVERSERVER_KUBECONFIG ]]
then
  echo "export KUBECONFIG=/etc/kubernetes/static-pod-resources/recovery-kube-apiserver-pod/admin.kubeconfig" >> ${ENV_REMOTE}
  ssh core@${MASTER_0} 'source /tmp/recovering-certs/env_remote.sh; sudo chmod 0644 /etc/kubernetes/static-pod-resources/recovery-kube-apiserver-pod/admin.kubeconfig'
  scp ${ENV_REMOTE} core@${MASTER_0}:/tmp/recovering-certs/env_remote.sh > /tmp/copy_env_remote.log
  ssh core@${MASTER_0} 'source /tmp/recovering-certs/env_remote.sh; sleep 60'
  if [[ $? -eq 0 ]]
  then
    echo -e "\n\e[1;32m STATUS: SUCCESSFUL => Recovery API Server provisioned \e[0m"
  else
  echo -e "\n\e[1;31m `cat /tmp/RECOVERSERVER_KUBECONFIG`"
  echo -e "\n\e[1;31m STATUS: FAILED => Recovery API Server not provisioned \e[0m"
  exit 1
  fi
else
echo -e "\n\e[1;31m `cat /tmp/RECOVERSERVER_KUBECONFIG`"
echo -e "\n\e[1;31m STATUS: FAILED => Recovery API Server not provisioned \e[0m"
exit 1
fi

# 5 - Regeneration kubernetes cetificates

echo -e "\n\e[1;34m 5 - REGENERATING CERTIFICATES WITH < regenerate-certificates > COMMAND. IT FIXES THE CERTIFICATES IN THE API, OVERWRITES THE OLD CERTIFICATES ON THE LOCAL DRIVE, AND RESTARTS STATIC PODs TO PICK THEM UP \e[0m"
echo -e "\e[1;34m Starting ... \e[0m\n"
ssh core@${MASTER_0} 'source /tmp/recovering-certs/env_remote.sh; sudo podman run -it --network=host -v /etc/kubernetes/:/etc/kubernetes/:Z --entrypoint=/usr/bin/cluster-kube-apiserver-operator "${KAO_IMAGE}" regenerate-certificates'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESS => certificates regenerated \e[0m"
else
  echo -e "\n\e[1;31m STATUS: FAILED => certificates not regenerated \e[0m"
  exit 1
fi

# 6 - Forcing new rollouts for the control plane

echo -e "\n \e[1;34m 6 - FORCING NEW ROLLOUTS FOR THE CONTROL PLANE. IT WILL REINSTALL ITSELF ON THE OTHER NODES \e[0m"
echo -e "\e[1;34m Starting ... \e[0m\n"

cat > /tmp/crt-rollouts.sh << EOF
source /tmp/recovering-certs/env_remote.sh
oc patch kubeapiserver cluster -p='{"spec": {"forceRedeploymentReason": "recovery-'"$( date --rfc-3339=ns )"'"}}' --type=merge
oc patch kubecontrollermanager cluster -p='{"spec": {"forceRedeploymentReason": "recovery-'"$( date --rfc-3339=ns )"'"}}' --type=merge
oc patch kubescheduler cluster -p='{"spec": {"forceRedeploymentReason": "recovery-'"$( date --rfc-3339=ns )"'"}}' --type=merge
EOF
scp /tmp/crt-rollouts.sh core@${MASTER_0}:/tmp/crt-rollouts.sh > /tmp/copy-crt-rollouts.log; ssh core@${MASTER_0} 'sudo chmod 0770 /tmp/crt-rollouts.sh; /tmp/crt-rollouts.sh'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESS => forcing new rollouts for the control plane \e[0m"
else
  echo -e "`cat /tmp/copy-crt-rollouts.log`"
  echo -e "\n\e[1;31m STATUS: FAILED => forcing new rollouts for the control plane \e[0m"
  exit 1
fi

# 7 - Creating a bootstrap kubeconfig

echo -e "\n\e[1;34m 7 - CREATING THE BOOTSTRAP KUBECONFIG FILE \e[0m"

echo -e "\n\e[1;34m 7.1 - Generating the bootstrap kubeconfig file with < recover-kubeconfig.sh > command \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
ssh core@${MASTER_0} 'source /tmp/recovering-certs/env_remote.sh; /usr/local/bin/recover-kubeconfig.sh > /tmp/recovering-certs/kubeconfig; sudo chown core /tmp/recovering-certs/kubeconfig'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => bootstrap kubeconfig created \e[0m"
else
  echo -e "\n\e[1;31m STATUS: FAILED => bootstrap kubeconfig not created \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 7.2 - Copying a bootstrap kubeconfig to master-0 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
ssh core@${MASTER_0} 'source /tmp/recovering-certs/env_remote.sh; sudo cp /tmp/recovering-certs/kubeconfig /etc/kubernetes/kubeconfig'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => bootstrap kubeconfig copy to master-0 \e[0m"
else
  echo -e "\n\e[1;31m STATUS: FAILED => bootstrap kubeconfig copy to master-0 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 7.3 - Copying a bootstrap kubeconfig to master-1 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
scp  core@${MASTER_0}:/tmp/recovering-certs/kubeconfig /tmp/kubeconfig > /tmp/kubeconfig_copy.log; scp /tmp/kubeconfig core@${MASTER_1}:/tmp/kubeconfig >> /tmp/kubeconfig_copy.log; ssh core@${MASTER_1} 'sudo cp /tmp/kubeconfig /etc/kubernetes/kubeconfig'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => bootstrap kubeconfig copy to master-1 \e[0m"
else
  echo -e "`cat /tmp/kubeconfig_copy.log`"
  echo -e "\n\e[1;31m STATUS: FAILED => bootstrap kubeconfig copy to master-1 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 7.4 - Copying a bootstrap kubeconfig to master-2 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
scp /tmp/kubeconfig core@${MASTER_2}:/tmp/kubeconfig > /tmp/kubeconfig_copy.log; ssh core@${MASTER_2} 'sudo cp /tmp/kubeconfig /etc/kubernetes/kubeconfig'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => bootstrap kubeconfig copy to master-2 \e[0m"
else
  echo -e "`cat /tmp/kubeconfig_copy.log`"
  echo -e "\n\e[1;31m STATUS: FAILED => bootstrap kubeconfig copy to master-2 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 7.5 - Copying a bootstrap kubeconfig to worker-0 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
scp /tmp/kubeconfig core@${WORKER_0}:/tmp/kubeconfig > /tmp/kubeconfig_copy.log; ssh core@${WORKER_0} 'sudo cp /tmp/kubeconfig /etc/kubernetes/kubeconfig'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => bootstrap kubeconfig copy to worker-0 \e[0m"
else
  echo -e "`cat /tmp/kubeconfig_copy.log`"
  echo -e "\n\e[1;31m STATUS: FAILED => bootstrap kubeconfig copy to worker-0 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 7.6 - Copying a bootstrap kubeconfig to worker-1 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
scp /tmp/kubeconfig core@${WORKER_1}:/tmp/kubeconfig > /tmp/kubeconfig_copy.log; ssh core@${WORKER_1} 'sudo cp /tmp/kubeconfig /etc/kubernetes/kubeconfig'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => bootstrap kubeconfig copy to worker-1 \e[0m"
else
  echo -e "`cat /tmp/kubeconfig_copy.log`"
  echo -e "\n\e[1;31m STATUS: FAILED => bootstrap kubeconfig copy to worker-1 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 7.7 - Copying a bootstrap kubeconfig to worker-2 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
scp /tmp/kubeconfig core@${WORKER_2}:/tmp/kubeconfig > /tmp/kubeconfig_copy.log; ssh core@${WORKER_2} 'sudo cp /tmp/kubeconfig /etc/kubernetes/kubeconfig'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => bootstrap kubeconfig copy to worker-2 \e[0m"
else
  echo -e "`cat /tmp/kubeconfig_copy.log`"
  echo -e "\n\e[1;31m STATUS: FAILED => bootstrap kubeconfig copy to worker-2 \e[0m"
  exit 1
fi

# 8 - Getting the CA certificate used to validate connections from the API server

echo -e "\n\e[1;34m 8 - GETTING THE CA CERTIFICATE USED TO VALIDATE CONNECTIONS FROM THE API SERVER \e[0m"

echo -e "\n\e[1;34m 8.1 - Get the CA certificate \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
cat > /tmp/getting-certs-from-api.sh << EOF
source /tmp/recovering-certs/env_remote.sh
oc get configmap kube-apiserver-to-kubelet-client-ca -n openshift-kube-apiserver-operator --template='{{ index .data "ca-bundle.crt" }}' > /tmp/recovering-certs/ca.crt
sudo chown core /tmp/recovering-certs/ca.crt
EOF
scp /tmp/getting-certs-from-api.sh core@${MASTER_0}:/tmp/getting-certs-from-api.sh > /tmp/copy-getting-certs-from-api.sh.log
ssh core@${MASTER_0} 'sudo chmod 0770 /tmp/getting-certs-from-api.sh; /tmp/getting-certs-from-api.sh'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => getting the CA certificate \e[0m"
else
  echo -e "`cat /tmp/copy-getting-certs-from-api.sh.log`"
  echo -e "\n\e[1;31m STATUS: FAILED => getting the CA certificate \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 8.2 - Copying CA certificate to master-0 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
ssh core@${MASTER_0} 'sudo cp /tmp/recovering-certs/ca.crt /etc/kubernetes/ca.crt'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => copy of the CA certificate to master-0 \e[0m"
else
  echo -e "\n\e[1;31m STATUS: FAILED => copy of the CA certificate to master-0 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 8.3 - Copying CA certificate to master-1 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
scp  core@${MASTER_0}:/tmp/recovering-certs/ca.crt /tmp/ca.crt > /tmp/copying_crt.log ; scp /tmp/ca.crt core@${MASTER_1}:/tmp/ca.crt >> /tmp/copying_crt.log; ssh core@${MASTER_1} 'sudo cp /tmp/ca.crt /etc/kubernetes/ca.crt'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => copy of the CA certificate to master-1 \e[0m"
else
  echo -e "`cat /tmp/copying_crt.log`"
  echo -e "\n\e[1;31m STATUS: FAILED => copy of the CA certificate to master-1 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 8.4 - Copying CA certificate to master-2 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
scp /tmp/ca.crt core@${MASTER_2}:/tmp/ca.crt > /tmp/copying_crt.log; ssh core@${MASTER_2} 'sudo cp /tmp/ca.crt /etc/kubernetes/ca.crt'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => copy of the CA certificate to master-2 \e[0m"
else
  echo -e "`cat /tmp/copying_crt.log`"
  echo -e "\n\e[1;31m STATUS: FAILED => copy of the CA certificate to master-2 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 8.5 - Copying CA certificate to worker-0 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
scp /tmp/ca.crt core@${WORKER_0}:/tmp/ca.crt > /tmp/copying_crt.log; ssh core@${WORKER_0} 'sudo cp /tmp/ca.crt /etc/kubernetes/ca.crt'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => copy of the CA certificate to worker-0 \e[0m"
else
  echo -e "`cat /tmp/copying_crt.log`"
  echo -e "\n\e[1;31m STATUS: FAILED => copy of the CA certificate to worker-0 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 8.6 - Copying CA certificate to worker-1 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
scp /tmp/ca.crt core@${WORKER_1}:/tmp/ca.crt > /tmp/copying_crt.log; ssh core@${WORKER_1} 'sudo cp /tmp/ca.crt /etc/kubernetes/ca.crt'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => copy of the CA certificate to worker-1 \e[0m"
else
  echo -e "`cat /tmp/copying_crt.log`"
  echo -e "\n\e[1;31m STATUS: FAILED => copy of the CA certificate to worker-1 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 8.7 - Copying CA certificate to worker-2 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
scp /tmp/ca.crt core@${WORKER_2}:/tmp/ca.crt > /tmp/copying_crt.log; ssh core@${WORKER_2} 'sudo cp /tmp/ca.crt /etc/kubernetes/ca.crt'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => copy of the CA certificate to worker-2 \e[0m"
else
  echo -e "`cat /tmp/copying_crt.log`"
  echo -e "\n\e[1;31m STATUS: FAILED => copy of the CA certificate to worker-2 \e[0m"
  exit 1
fi

# 9 - Add the machine-config-daemon-force file to all master hosts and nodes to force the Machine Config Daemon to accept this certificate update

echo -e "\n\e[1;34m 9 - ADDING THE < machine-config-daemon-force > FILE TO MASTER AND WORKERS NODES \e[0m"

echo -e "\n\e[1;34m 9.1 - Adding the machine-config-daemon-force file to master-0 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
ssh core@${MASTER_0} 'sudo touch /run/machine-config-daemon-force'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => add of the machine-config-daemon-force file to master-0 \e[0m"
else
  echo -e "\n\e[1;31m STATUS: FAILED add of the machine-config-daemon-force file to master-0 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 9.2 - Adding the machine-config-daemon-force file to master-1 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
ssh core@${MASTER_1} 'sudo touch /run/machine-config-daemon-force'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => add of the machine-config-daemon-force file to master-1 \e[0m"
else
  echo -e "\n\e[1;31m STATUS: FAILED => add of the machine-config-daemon-force file to master-1 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 9.3 - Adding the machine-config-daemon-force file to master-2 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
ssh core@${MASTER_2} 'sudo touch /run/machine-config-daemon-force'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => add of the machine-config-daemon-force file to master-2 \e[0m"
else
  echo -e "\n\e[1;31m STATUS: FAILED => add of the machine-config-daemon-force file to master-2 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 9.4 - Adding the machine-config-daemon-force file to worker-0 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
ssh core@${WORKER_0} 'sudo touch /run/machine-config-daemon-force'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => add of the machine-config-daemon-force file to worker-0 \e[0m"
else
  echo -e "\n\e[1;31m STATUS: FAILED => add of the machine-config-daemon-force file to worker-0 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 9.5 - Adding the machine-config-daemon-force file to worker-1 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
ssh core@${WORKER_1} 'sudo touch /run/machine-config-daemon-force'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => add of the machine-config-daemon-force file to worker-1 \e[0m"
else
  echo -e "\n\e[1;31m STATUS: FAILED => add of the machine-config-daemon-force file to worker-1 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 9.6 - Adding the machine-config-daemon-force file to worker-2 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
ssh core@${WORKER_2} 'sudo touch /run/machine-config-daemon-force'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => add of the machine-config-daemon-force file to worker-2 \e[0m"
else
  echo -e "\n\e[1;31m STATUS: FAILED => add of the machine-config-daemon-force file to worker-2 \e[0m"
  exit 1
fi

# 10 - Recovering the kubelet on all masters and worker

echo -e "\n\e[1;34m 10 - RECOVERING THE KUBELET IN MASTERS AND WORKERS \e[0m"

echo -e "\n\e[1;34m 10.1 - Recovering the kubelet in master-0 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
echo -e "\e[1;34m (it might take several minutes) \e[0m"
ssh core@${MASTER_0} 'sudo systemctl stop kubelet; sudo rm -rf /var/lib/kubelet/pki /var/lib/kubelet/kubeconfig; sudo systemctl start kubelet'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => recovering the kubelet in master-0 \e[0m"
else
  echo -e "\n\e[1;31m STATUS: FAILED => recovering the kubelet in master-0 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 10.2 - Recovering the kubelet in master-1 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
ssh core@${MASTER_1} 'sudo systemctl stop kubelet; sudo rm -rf /var/lib/kubelet/pki /var/lib/kubelet/kubeconfig; sudo systemctl start kubelet'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => recovering the kubelet in master-1 \e[0m"
else
  echo -e "\n\e[1;31m STATUS: FAILED => recovering the kubelet in master-1 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 10.3 - Recovering the kubelet in master-2 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
ssh core@${MASTER_2} 'sudo systemctl stop kubelet; sudo rm -rf /var/lib/kubelet/pki /var/lib/kubelet/kubeconfig; sudo systemctl start kubelet'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => recovering the kubelet in master-2 \e[0m"
else
  echo -e "\n\e[1;31m STATUS: FAILED => recovering the kubelet in master-2 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 10.4 - Recovering the kubelet in worker-0 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
ssh core@${WORKER_0} 'sudo systemctl stop kubelet; sudo rm -rf /var/lib/kubelet/pki /var/lib/kubelet/kubeconfig; sudo systemctl start kubelet'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => recovering the kubelet in worker-0 \e[0m"
else
  echo -e "\n\e[1;31m STATUS: FAILED => recovering the kubelet in worker-0 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 10.5 - Recovering the kubelet in worker-1 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
ssh core@${WORKER_1} 'sudo systemctl stop kubelet; sudo rm -rf /var/lib/kubelet/pki /var/lib/kubelet/kubeconfig; sudo systemctl start kubelet'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => recovering the kubelet in worker-1 \e[0m"
else
  echo -e "\n\e[1;31m STATUS: FAILED => recovering the kubelet in worker-1 \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 10.6 - Recovering the kubelet in worker-2 \e[0m"
echo -e "\e[1;34m Starting ... \e[0m"
ssh core@${WORKER_2} 'sudo systemctl stop kubelet; sudo rm -rf /var/lib/kubelet/pki /var/lib/kubelet/kubeconfig; sudo systemctl start kubelet'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => recovering the kubelet in worker-2 \e[0m"
else
  echo -e "\n\e[1;31m STATUS: FAILED => recovering the kubelet in worker-2 \e[0m"
  exit 1
fi

# 11 - Approve the pending node-bootstrapper certificates signing requests (CSRs)

echo -e "\n\e[1;34m 11 - APPROVING CERTIFICATES SIGNING REQUESTS (CSRs) \e[0m"
echo -e "\e[1;34m Starting ... \e[0m\n"
ssh core@${MASTER_0} 'source /tmp/recovering-certs/env_remote.sh; oc get csr -o name | xargs -n 1 oc adm certificate approve'
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;34m STATUS: Still approving the pending node-bootstrapper certificates signing requests (CSRs) \e[0m\n"
  sleep 5;
  ssh core@${MASTER_0} 'source /tmp/recovering-certs/env_remote.sh; oc get csr -o name | xargs -n 1 oc adm certificate approve'
  echo -e "\n\e[1;32m STATUS: SUCCESFUL => approving the pending node-bootstrapper certificates signing requests (CSRs) \e[0m"
else
  echo -e "\n\e[1;31m STATUS: FAILED => approving the pending node-bootstrapper certificates signing requests (CSRs) \e[0m\n"
  exit 1
fi

echo -e "\n\e[1;34m 11.1 - Depuring previous Recovery API Server \e[0m"
echo -e "\e[1;34m Starting ...  \e[0m"
ssh core@${MASTER_0} 'source /tmp/recovering-certs/env_remote.sh; sudo podman run -it --network=host -v /etc/kubernetes/:/etc/kubernetes/:Z --entrypoint=/usr/bin/cluster-kube-apiserver-operator "${KAO_IMAGE}" recovery-apiserver destroy' > /tmp/recoveryApiServerDestroy.log
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;34m INFO: successful destroy of Recovery API Server \e[0m"
else
  echo -e "\n\e[1;34m INFO: Recovery API Server not found \e[0m"
fi

echo -e "\n\e[1;32m 12 - END RECOVERY STATUS => COMPLETED \e[0m"
echo -e "\n\e[1;32m `date` \e[0m"
echo -e "\n\e[7m Please WAIT 10 min to resume OCP tasks, the control plane needs to restart and pick up the new certificates \e[0m\n"

# END


# RECOVERING FROM EXPIRED CONTROL PLANE CERTIFICATES

### Follow this procedure to recover from a situation where the control plane certificates have expired.

Prerequisites:

- Working kubeconfig file
- SSH keys to master and worker nodes
- A bastion host with access to masters and worker nodes

STEPS:

1 - Download the git repo to the bastion host

```$ git clone https://gitlab.com/rcardona/recovering-from-expired-control-plane-certificates-ocp-v4.3.git```

```$ cd recovering-from-expired-control-plane-certificates-ocp-v4.3```

2 - Update the <env_local.sh> with the needed variables. Start this step by executing as follows

```$ ./get_OPERATOR_IMAGE_VERSION```

3 - Once all the needed variables are set in <env_local.sh>, start the recovering of control plane certificates

```$ ./recovering-expired-certs-ocp-v4.3.sh```

4 - First login after recovering (use original kubeadmin credentials)
```$ oc login https://api.${CLUSTERNAME}.${DOMAIN}:6443```

##### Example of how to do it.
![OCP Recovering from expired certs](media/recovering-expired-certs.mp4)
##### NOTE: In case the recovering needs to takes place again, be sure that the dynamic created variables are removed from the env_remote.sh file

#!/usr/bin/env bash

# PLEASE DO NOT CHANGE THIS VARIABLE
export ENV_REMOTE="env_remote.sh"


# This variable should point to the latest working kubeconf/config file
# Ex. export KUBECONFIG_FILE="/root/.kube/config"
export KUBECONFIG_FILE=""

# Ex. export MASTER_0="10.10.10.11"
export MASTER_0=""
export MASTER_1=""
export MASTER_2=""

export WORKER_0=""
export WORKER_1=""
export WORKER_2=""

# This varible could be found by executing get_OPERATOR_IMAGE_VERSION.sh script
# Ex. export RELEASE_IMAGE="4.3.5"
export RELEASE_IMAGE=""

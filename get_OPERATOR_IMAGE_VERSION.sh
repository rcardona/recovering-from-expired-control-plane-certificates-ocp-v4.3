#!/bin/bash
source env_local.sh

if [[ -z $KUBECONFIG_FILE ]]
then
  echo -e "\n\e[1;31m STATUS: FAILED => varible KUBECONFIG_FILE yet unset, please set in env_local.sh \e[0m\n"
  exit 1
else
  echo -e "\n\e[1;32m STATUS: SUCCESS => varible KUBECONFIG_FILE set \e[0m"
fi

if [[ -z $MASTER_0 ]]
then
  echo -e "\n\e[1;31m STATUS: FAILED => varible MASTER_0 yet unset, please set in env_local.sh \e[0m\n"
  exit 1
else
  echo -e "\n\e[1;32m STATUS: SUCCESS => varible MASTER_0 set \e[0m"
fi

echo -e "\n\e[1;34m 0 - COPYING OCP ENVIRONMENT TO MASTER-0 \e[0m"
echo -e "\e[1;34m Starting ...  \e[0m"
ssh core@${MASTER_0} 'mkdir -p /tmp/recovering-certs/.kube'
scp ${KUBECONFIG_FILE} core@${MASTER_0}:/tmp/recovering-certs/.kube/config > /tmp/kubeconfig_copy.log
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESS => kubeconfig/config file successfully copied to master-0 \e[0m"
else
  echo -e "\n\e[1;31m `cat /tmp/kubeconfig_copy.log`"
  echo -e "\e[1;31m STATUS: FAILED => kubeconf/config not found (${KUBECONFIG_FILE}) \e[0m\n"
  exit 1
fi

echo -e "\n\e[1;34m 1 - GETTING THE OPERATOR_IMAGE_VERSION \e[0m"

echo -e "\n\e[1;34m 1.1 - Copying recovery remote environment file to master-0 \e[0m"
echo -e "\e[1;34m Starting ...  \e[0m"
echo "export RELEASE_IMAGE=quay.io/openshift-release-dev/ocp-release:${RELEASE_IMAGE}-x86_64" >> ${ENV_REMOTE}
scp ${ENV_REMOTE} core@${MASTER_0}:/tmp/recovering-certs/env_remote.sh > /tmp/copy_env_remote.log
if [[ $? -eq 0 ]]
then
  echo -e "\n\e[1;32m STATUS: SUCCESS => recovery remote environment file successfully copied to master-0 \e[0m"
else
  echo -e "\n\e[1;31m `cat /tmp/copy_env_remote.log`"
  echo -e "\e[1;31m STATUS: FAILED => recovery remote environment file not found (${ENV_REMOTE}) \e[0m"
  exit 1
fi

echo -e "\n\e[1;34m 1.2 - Getting the cluster-operator-image-version \e[0m"
echo -e "\e[1;34m Starting ...  \e[0m \n"
ssh core@${MASTER_0} 'source /tmp/recovering-certs/env_remote.sh; oc get pod -n openshift-kube-apiserver-operator -o yaml | grep -A1 OPERATOR_IMAGE_VERSION' > /tmp/OPERATOR_IMAGE_VERSION.log
if [[ $? -eq 0 ]]
then
  echo -e "`cat /tmp/OPERATOR_IMAGE_VERSION.log`"
  echo -e "\n\e[1;32m STATUS: SUCCESS => cluster-kube-apiserver-operator image found \e[0m\n"
else
  echo -e "\n\e[1;31m STATUS: FAILED => cluster-kube-apiserver-operator image not found \e[0m\n"
  exit 1
fi
